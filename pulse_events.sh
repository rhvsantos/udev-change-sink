#!/bin/bash
LOG="$HOME/.pulse_events.log"
_BT_CONNECTED=$(pactl list sinks short | grep bluez | awk '{print$2}')

# If _BT_CONNECTED has no value, I'm defining a default value for this variable that is my Notebook Speaker Sink
_SETTING_DEFAULT_VALUE=${_BT_CONNECTED:="alsa_output.pci-0000_00_1f.3-platform-skl_hda_dsp_generic.HiFi__hw_sofhdadsp__sink"}

# Removing old log
rm -rf "$LOG"

# Defining a new default_sink
echo "Defining $_BT_CONNECTED as default-sink..." >> $LOG
pactl set-default-sink $_BT_CONNECTED

# Moving sources-sinks
echo "Defining source-sink to $_BT_CONNECTED" >> $LOG
for SINK_INPUT_NUMBER in $(pactl list sink-inputs short | awk '{print$1}')
do
	pactl move-sink-input $SINK_INPUT_NUMBER $_BT_CONNECTED
done

echo 

OUTPUTS=$(pactl list source-outputs short | cut -f 1)
for i in $OUTPUTS; do
	pactl move-source-output $i "$_BT_CONNECTED.monitor"
	echo "Moving source-output $i to $_BT_CONNECTED" >> $LOG
done


exit 0

