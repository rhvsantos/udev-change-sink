I have made this script because I was having problems with sound in my Arch Linux (With bspwm and no DE), mainly when I was connecting/switching a Bluetooth speaker/headphone device, I stayed with no audio in the most of the time and I manually have to switch between my Bluetooth Device and Notebook Speaker.

###### How to Use:

First you need to identify your "Notebook speaker Sink", after doing this, just edit the variable "_SETTING_DEFAULT_VALUE" in this script with your Sink. To do this, you can run the follow command:

$ pactl list sinks short

#### Example of my output
![Image1](/images-examples/image1.png)

After this, you should configure a rule in your udev to run this script when a bluetooth device is connected. For example:

###### Rule file: /etc/udev/rules.d/90-bluetooth-sink-add.rule
ACTION=="add", SUBSYSTEM=="input", ATTRS{phys}=="FF:FF:FF:FF:FF:FF", RUN+="/bin/sh /home/<your-user>/pulse_events.sh"

###### Rule file: /etc/udev/rules.d/95-bluetooth-sink-remove.rule
ACTION=="remove", SUBSYSTEM=="input", ATTRS{phys}=="FF:FF:FF:FF:FF:FF", RUN+="/bin/sh /home/<your-user>/pulse_events.sh"

* Where "FF:FF:FF:FF:FF:FF" Is the MAC Address of my Headphone

###### You can get the Mac Address running:
$ udevadm info -ap <Device Path>

* You can get the Device Path running udevadm monitor while you connect or disconnect your bluetooth device, example of a device patch: /devices/virtual/input/input36

> udevadm monitor --property --kernel
